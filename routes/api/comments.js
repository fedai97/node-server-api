const router = require('express').Router();
const Comments = require('../../models/comment');

router.route('/')
    .get(async function (req, res) {
        try {
            const product = req.query.product || null;
            const filter = {};

            if (product) {
                filter.product = product;
            }

            const comments = await Comments.find(filter);

            await res.json(comments);
        } catch (err) {
            return res.status(500).send(err);
        }
    })
    .post(async function (req, res) {
        try {
            const newData = new Comments({
                title: req.body.title,
                description: req.body.description,
                product: req.body.product,
                created_at: new Date(),
                updated_at: new Date()
            });

            await newData.save();
            await res.json(newData);
        } catch (err) {
            return res.status(500).send(err);
        }
    });
router.route('/:commentId')
    .get(async function (req, res) {
        try{
            const comment = await Comments.find({_id:req.params.commentId});

            await res.json(comment);
        } catch (err) {
            return res.status(500).send(err);
        }
    })
    .put(function (req, res) {
        //    TODO-- post
    })
    .delete(async function (req, res) {
        try {
            const delData = await Comments.findByIdAndRemove(req.params.commentId);

            await res.json(delData);
        } catch (err) {
            return res.status(500).send(err);
        }
    });

module.exports = router;