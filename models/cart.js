const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CartSchema = new Schema({
    product: { type: Schema.Types.ObjectId, ref: 'Product' },
    price: {type: Number, default: 0},
    amount: {type: Number, default: 1},
    sessionId: {type: String},
    created_at: Date,
    updated_at: Date,
});

const Cart = mongoose.model('Cart', CartSchema);

module.exports = Cart;