const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
    title: String,
    description: String,
    category: { type: Schema.Types.ObjectId, ref: 'Category' },
    path: {type: String, default: ''},
    created_at: Date,
    updated_at: Date,
});

const Category = mongoose.model('Category', CategorySchema);

module.exports = Category;