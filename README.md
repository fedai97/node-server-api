# Node Server API

# Product
1. Add product
```json
"type": "POST"
"url": "http://localhost:3000/api/products"
"data":{
     "title": "String"
     "description": "String"
     "img": "String"
     "category": "String"
     "price": "Number"
}
```
2. Delete product
```json
"type": "DELETE"
"url": "http://localhost:3000/api/products/:productId"
```
3. Get all products
```json
"type": "GET"
"url": "http://localhost:3000/api/products"
```

# Category
1. Add category
```json
"type": "POST"
"url": "http://localhost:3000/api/categories"
"data":{
      "title": "String"
      "description": "String"
      "category": "String"
      "path": "String"
}
```
2. Delete category
```json
"type": "DELETE"
"url": "http://localhost:3000/api/categories/:categoryId"
```
3. Get all categories
```json
"type": "GET"
"url": "http://localhost:3000/api/categories"
```

# Cart
1. Add product to cart
```json
"type": "POST"
"url": "http://localhost:3000/api/carts"
"data":{
    "productId": "String"
    "price": "Number"
    "amount": "Number"
    "sessionId": "String"
}
```
2. Delete cart
```json
"type": "DELETE"
"url": "http://localhost:3000/api/carts/:cardId"
```
3. Get all cart
```json
"type": "GET"
"url": "http://localhost:3000/api/carts"
```

# Comment
1. Add comment
```json
"type": "POST"
"url": "http://localhost:3000/api/comments"
"data":{
       "title": "String"
       "description": "String"
       "product": "String"
}
```
2. Delete comment
```json
"type": "DELETE"
"url": "http://localhost:3000/api/comments/:commentId"
```
3. Get  all comments
```json
"type": "GET"
"url": "http://localhost:3000/api/comments"
```

# Slider
1. Add slider
```json
"type": "POST"
"url": "http://localhost:3000/api/sliders"
"data":{
        "title": "String"
        "description": "String"
        "image": "String"
        "position": "Number"
}
```
2. Delete slider
```json
"type": "DELETE"
"url": "http://localhost:3000/api/sliders/:sliderId"
```
3. Get all sliders
```json
"type": "GET"
"url": "http://localhost:3000/api/sliders"
```
